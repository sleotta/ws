﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Weiss {
    [CreateAssetMenu(menuName = "Managers/Resource Manager")]
    public class ResourcesManager : ScriptableObject
    {
        public Element typeElement;
    }
}
