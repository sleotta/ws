﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Weiss {
    [CreateAssetMenu(menuName = "CardType/Event")]
    public class EventCard : CardType
    {
        public override void OnSetType(CardViz viz) {
            base.OnSetType(viz);
            viz.currentPower.SetActive(false);
        }
    }
}

