﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Weiss {

    [System.Serializable]
    public class CardProperties
    {
       public string stringValue;
       public int intValue;
       public Sprite sprite;
       public Element element;
    }

}
