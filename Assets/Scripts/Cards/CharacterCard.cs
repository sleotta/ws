﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Weiss {
    [CreateAssetMenu(menuName = "CardType/Character")]
    public class CharacterCard : CardType
    {
        public override void OnSetType(CardViz viz) {
            base.OnSetType(viz);
            viz.currentPower.SetActive(true);
        }
    }

}
